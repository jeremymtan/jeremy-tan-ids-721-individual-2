[![pipeline status](https://gitlab.com/jeremymtan/jeremy-tan-ids-721-individual-2/badges/main/pipeline.svg)](https://gitlab.com/jeremymtan/jeremy-tan-ids-721-individual-2/-/commits/main)
# Simple REST API Semantic Search on Vector Database
## Purpose of Project 
The purpose of the project is to do data ingestion into a vector database, peform queries and viz (I do this by finding the top 2 similar animials that are related to my sentence), and visualize the output to the user when they access my api. I combine my mini-7 and individual 2 to provide an easy way to let users access my vector databse via a `GET` request by letting them access a public endpoint throught the API gateway. I fork this project from my mini-7 as I completed both as I found it as a useful example to create a SIMPLE REST API. 

## Youtube Video

https://youtu.be/BNWp1gtSdAE

## Qdrant Collection 

![Screenshot_2024-03-24_at_1.59.15_PM](/uploads/2492e4b39ed63a1d26ded1e1781adc4e/Screenshot_2024-03-24_at_1.59.15_PM.png)


## Sample Viz 
My query is "A large aquatic sea animal": `https://af154b8hxj.execute-api.us-east-1.amazonaws.com/no/yes?q=%27large%20aquatic%20sea%20animal%27`

![Screenshot_2024-03-24_at_2.00.26_PM](/uploads/8749c53ca9e75c5449a376f51793318d/Screenshot_2024-03-24_at_2.00.26_PM.png)

## Docker Image in ECR Container Registry
I use my Dockerfile to extract the binary file of my project and upload it to AWS ECR Container Registry

![Screenshot_2024-03-24_at_2.03.12_PM](/uploads/ceb75d42d505757b06e252a80fd115a9/Screenshot_2024-03-24_at_2.03.12_PM.png)

## CICD pipeline to Push to ECR Contianer Which is Linked to Lambda Function

![Screenshot_2024-03-24_at_2.04.31_PM](/uploads/d217a23b4b71fdeddd9b9110eebb8172/Screenshot_2024-03-24_at_2.04.31_PM.png)

## Lambda Architecture Diagram 
Data ingestion is done through `setup.rs` and Cohere API into a qdrant collection via the qdrant API

![Screenshot_2024-03-24_at_2.05.23_PM](/uploads/f9e370c56f630a7b7ab526244d914ea9/Screenshot_2024-03-24_at_2.05.23_PM.png)

## Preparation
1. Sign up for qdrant and create a cluster to have access to a vector database and grab api key and cluster url
2. Sign up for cohere and grab api key to generate embeddings for vector database 
3. Create a new project `cargo lambda new <insert_folder name>`
4. Store your api keys into a `.env` file 
5. Once you have the `.env` file setup and you export each variable, move on data ingestion 
6. In my `setup.rs` script, I read a `.jsonl` file of various animals to pass into data ingestion 
7. Since this is a seperate package, I momentarily add bins to my cargo.toml so I can run the file seperate from my main.rs (I comment them out later on to make it easier to debug deployment)
8. Run your bin to do data ingenstion for me it's `cargo run --bin setup_collection animals.jsonl`
9. Your qdrant cluster should now have a collection filled with associated embeddings or `points`
10. Make visualizations and aggregations in main.rs (I do semantic search and visualize the top two related rows in my db based on a query sentence)
11. Do `cargo lambda watch` to test locally.
12. Test you viz and aggreation works by sending a GET request: `http://localhost:9000/?tianji='large sea animal'`
13. Once you confirm that you now need to set up an IAM user to be able to create the lambda function 
14. Attach policies `IAMFullAccess`, `AmazonEC2ContainerRegistryFullAccess`, and `AWSLambda_FullAccess`.
15. Finish user creation, open user details, and go to security credentials.
16. Generate access key (choose OTHER).
17. Store AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION (your choice) in your .env file that won't be pushed with your repo (add to .gitignore)
18. Export the variables again 
19. Build your project for release to make sure it works `cargo lambda build --release`
20. You can either make sure your API gateway works without an docker iamge for testing or skip to step 28
21. Deploy your project for release `cargo lambda deploy --env-file .env`
22. Login to AWS Lambda (make sure you have the region correct on the top right bar) and check you have it installed.
23. After confirmation, you now want to connect your Lambda function with AWS API Gateway so head over there.
24. Create a new API (keep default settings, REST API) then create a new resource (the URL path that will be appended to your API link) and enable CORS option (since you connect to different APIs outside of AWS).
25. Create a method to attach the lambda function (since this is an http lambda function, turn on lambda proxy integration)
26. Deploy stage, then test your url 
27. If it works, you can now try building an image and attaching it. 
28. Go to ECR regristry and create a new private registry. Copy the commands to login. 
29. Start Docker, copy my Dockerfile, and do `docker buildx build --progress=plain --platform linux/arm64 -t week7 .`
30. Then follow the steps in the ECR guide to push to ECR repo
31. Go to lambda, create lambda function using image, then search for your image, choose arm64 in the options then proceed
32. Since you made a new function, you would need to add your environmnet variables in the configuration of your lambda fucntion (for me they are `QDRANT_URI`, `QDRANT_KEY`, and `COHERE_KEY`)
33. Follow steps 24-26

## References
Mans made it so hard to connect to qdrant with a port. put it at the end of the README and expects me to catch it
1. https://github.com/qdrant/rust-client
2. https://stackoverflow.com/questions/34612395/openssl-crate-fails-compilation-on-mac-os-x-10-11
3. https://github.com/qdrant/internal-examples/blob/master/lambda-search/src/setup_collection.rs
4. https://medium.com/@jed.lechner/effortless-guide-to-setting-up-aws-lambda-with-rust-b2630eeaa0f0
5. https://stackoverflow.com/questions/20813486/exploring-docker-containers-file-system
6. https://gist.githubusercontent.com/florentchauveau/2dc4da0299d42258606fc3b0e148fc07/raw/86fd64c514364800209180ec78d2704070a3c7b6/.gitlab-ci.yml
7. https://stackoverflow.com/questions/62169568/docker-alpine-linux-python-missing
8. https://docs.cohere.com/docs/the-cohere-platform

